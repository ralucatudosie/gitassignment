package ro.ase.csie.cts.gr1084.tema2;

public interface CalculPret {

	public static float pretMasinaPePerioadaInchirierii(int nrZileInchiriere, float pretPeZi)
	{
		return nrZileInchiriere * pretPeZi;
	}
}
