package ro.ase.csie.cts.gr1084.tema2;



public class Masina implements CalculPret{

	int cod;
	int nrLocuri;
	int zileInchiriere;
	float pretPeZi;
	
	public Masina(int cod, int nrLocuri, int zileInchiriere, float pretPeZi) {
		super();
		this.cod = cod;
		this.nrLocuri = nrLocuri;
		this.zileInchiriere = zileInchiriere;
		this.pretPeZi = pretPeZi;
	}
	
	float getPretPerioadaInchiriere()
	{
		return CalculPret.pretMasinaPePerioadaInchirierii(this.zileInchiriere, this.pretPeZi);
	}
	

	@Override
	public String toString() {
		return "Masina [cod=" + cod + ", nrLocuri=" + nrLocuri + ", zileInchiriere=" + zileInchiriere + ", pretPeZi="
				+ pretPeZi + ", pretPerioadaInchiriere=" + getPretPerioadaInchiriere() + "]";
	}
	
	
	
	
}
